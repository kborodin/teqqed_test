package com.example.teqqed_test.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.teqqed_test.model.datastorage.PostData
import kotlinx.coroutines.flow.Flow

@Dao
interface PostsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<PostData>)

    @Query("SELECT * FROM post")
    fun getPosts(): Flow<List<PostData>>

    @Query("SELECT * FROM post WHERE post.id = :postId")
    fun getPostById(postId: Int): Flow<PostData>
}