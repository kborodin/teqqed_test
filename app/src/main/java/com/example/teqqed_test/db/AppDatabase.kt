package com.example.teqqed_test.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.teqqed_test.db.dao.CommentsDao
import com.example.teqqed_test.db.dao.PostsDao
import com.example.teqqed_test.db.dao.UsersDao
import com.example.teqqed_test.model.datastorage.CommentData
import com.example.teqqed_test.model.datastorage.PostData
import com.example.teqqed_test.model.datastorage.UserData


@Database(
    entities = [PostData::class, UserData::class, CommentData::class],
    version = 1
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun postsDao(): PostsDao

    abstract fun usersDao(): UsersDao

    abstract fun commentsDao(): CommentsDao
}