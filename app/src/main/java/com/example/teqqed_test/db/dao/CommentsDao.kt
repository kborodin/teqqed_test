package com.example.teqqed_test.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.teqqed_test.model.datastorage.CommentData
import kotlinx.coroutines.flow.Flow

@Dao
interface CommentsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<CommentData>)

    @Query("SELECT * FROM comment WHERE comment.postId = :postId")
    fun getCommentsByPostId(postId: Int): Flow<List<CommentData>>
}