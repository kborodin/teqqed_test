package com.example.teqqed_test.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.teqqed_test.model.datastorage.UserData
import kotlinx.coroutines.flow.Flow


@Dao
interface UsersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<UserData>)

    @Query("SELECT * FROM user WHERE user.id = :userId")
    fun getUserById(userId: Int): Flow<UserData>
}