package com.example.teqqed_test.interactor

import com.example.teqqed_test.repository.user.UsersRepository
import javax.inject.Inject

class GetUserInteractor @Inject constructor(
    private val usersRepository: UsersRepository
) {

    fun execute(userId: Int) = usersRepository.getUserById(userId)
}