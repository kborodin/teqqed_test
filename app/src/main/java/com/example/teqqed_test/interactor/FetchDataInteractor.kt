package com.example.teqqed_test.interactor

import com.example.teqqed_test.api.TeqqedApi
import com.example.teqqed_test.repository.comment.CommentsRepository
import com.example.teqqed_test.repository.post.PostsRepository
import com.example.teqqed_test.repository.user.UsersRepository
import kotlinx.coroutines.flow.combine
import javax.inject.Inject

class FetchDataInteractor @Inject constructor(
    private val teqqedApi: TeqqedApi,
    private val postsRepository: PostsRepository,
    private val usersRepository: UsersRepository,
    private val commentsRepository: CommentsRepository
) {

    fun execute() =
        combine(
            teqqedApi.fetchPosts(),
            teqqedApi.fetchUsers(),
            teqqedApi.fetchComments()
        ) { posts, users, comments ->
            postsRepository.save(posts)
            usersRepository.save(users)
            commentsRepository.save(comments)
        }
}