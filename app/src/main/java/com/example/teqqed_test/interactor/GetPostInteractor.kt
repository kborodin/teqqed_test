package com.example.teqqed_test.interactor

import com.example.teqqed_test.repository.post.PostsRepository
import javax.inject.Inject

class GetPostInteractor @Inject constructor(
    private val postsRepository: PostsRepository
) {
    fun execute(postId: Int) = postsRepository.getPostById(postId)
}