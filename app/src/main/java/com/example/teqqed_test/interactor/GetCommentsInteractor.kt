package com.example.teqqed_test.interactor

import com.example.teqqed_test.repository.comment.CommentsRepository
import javax.inject.Inject

class GetCommentsInteractor @Inject constructor(
    private val commentsRepository: CommentsRepository
) {

    fun execute(postId: Int) =
        commentsRepository.getCommentsByPostId(postId)
}