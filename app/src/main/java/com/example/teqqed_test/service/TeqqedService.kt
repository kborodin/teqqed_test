package com.example.teqqed_test.service

import com.example.teqqed_test.model.api.CommentModel
import com.example.teqqed_test.model.api.PostModel
import com.example.teqqed_test.model.api.UserModel
import retrofit2.http.GET

interface TeqqedService {
    companion object {
        const val ENDPOINT = "https://jsonplaceholder.typicode.com/"
        const val POSTS = "posts"
        const val USERS = "users"
        const val COMMENTS = "comments"
    }

    @GET(POSTS)
    suspend fun fetchPosts(): List<PostModel>

    @GET(USERS)
    suspend fun fetchUsers(): List<UserModel>

    @GET(COMMENTS)
    suspend fun fetchComments(): List<CommentModel>
}