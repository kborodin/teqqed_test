package com.example.teqqed_test

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

interface AppDispatchers {
    val io: CoroutineDispatcher
    val mainThread: CoroutineDispatcher
}

data class DefaultAppDispatchers(
    override val io: CoroutineDispatcher = Dispatchers.IO,
    override val mainThread: CoroutineDispatcher = Dispatchers.Main
) : AppDispatchers