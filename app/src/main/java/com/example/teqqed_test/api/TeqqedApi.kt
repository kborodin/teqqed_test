package com.example.teqqed_test.api

import com.example.teqqed_test.model.domain.Comment
import com.example.teqqed_test.model.domain.Post
import com.example.teqqed_test.model.domain.User
import kotlinx.coroutines.flow.Flow

interface TeqqedApi {

    fun fetchPosts(): Flow<List<Post>>

    fun fetchUsers(): Flow<List<User>>

    fun fetchComments(): Flow<List<Comment>>
}