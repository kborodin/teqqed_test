package com.example.teqqed_test.api

import com.example.teqqed_test.AppDispatchers
import com.example.teqqed_test.model.domain.Comment
import com.example.teqqed_test.model.domain.Post
import com.example.teqqed_test.model.domain.User
import com.example.teqqed_test.model.toDomain
import com.example.teqqed_test.service.TeqqedService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class TeqqedApiImpl @Inject constructor(
    private val teqqedService: TeqqedService,
    private val appDispatchers: AppDispatchers
) : TeqqedApi {

    override fun fetchPosts(): Flow<List<Post>> =
        callApi(appDispatchers.io) {
            teqqedService.fetchPosts()
        }.map { list -> list.map { post -> post.toDomain() } }

    override fun fetchUsers(): Flow<List<User>> =
        callApi(appDispatchers.io) {
            teqqedService.fetchUsers()
        }.map { list -> list.map { user -> user.toDomain() } }

    override fun fetchComments(): Flow<List<Comment>> =
        callApi(appDispatchers.io) {
            teqqedService.fetchComments()
        }.map { list -> list.map { comment -> comment.toDomain() } }
}