package com.example.teqqed_test.api

import retrofit2.HttpException
import java.io.IOException


open class AppError(val code: ErrorCode, message: String? = null) : RuntimeException(message)

open class UnknownAppError(override val cause: Throwable) : AppError(ErrorCode.Unknown)


class NetworkError : AppError(ErrorCode.NetworkError)

class ServerFailureError : AppError(ErrorCode.ServerFailure)


enum class ErrorCode {
    NetworkError,
    ServerFailure,
    Unknown
}

internal fun Throwable.toDomain(): Throwable =
    when (this) {
        is HttpException -> toDomain()
        is IOException -> NetworkError()
        else -> UnknownAppError(this)
    }

internal fun HttpException.toDomain(): Throwable =
    when (code()) {
        500 -> ServerFailureError()
        else -> UnknownAppError(this)
    }



