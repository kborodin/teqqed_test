package com.example.teqqed_test.ui.posts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teqqed_test.interactor.FetchDataInteractor
import com.example.teqqed_test.interactor.GetPostsInteractor
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject

class PostsViewModel @Inject constructor(
    private val getPostsInteractor: GetPostsInteractor,
    fetchDataInteractor: FetchDataInteractor
) : ViewModel() {

    val postsViewData by lazy { PostsViewData() }

    init {
        fetchDataInteractor.execute()
            .catch { throwable -> Timber.e(throwable, "Could not get all data") }
            .mapLatest { getPosts() }
            .launchIn(viewModelScope)
    }

    private fun getPosts() =
        getPostsInteractor.execute()
            .mapLatest { postsViewData.postsList.postValue(it) }
            .catch { throwable -> Timber.e(throwable, "Could not get posts list") }
            .launchIn(viewModelScope)
}