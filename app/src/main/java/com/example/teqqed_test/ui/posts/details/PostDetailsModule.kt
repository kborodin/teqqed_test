package com.example.teqqed_test.ui.posts.details

import androidx.lifecycle.ViewModel
import com.example.teqqed_test.di.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
abstract class PostDetailsModule {
    companion object {

        @Provides
        fun providePostId(fragment: PostDetailsFragment): Int? = fragment.args.postId
    }

    @Binds
    @IntoMap
    @ViewModelKey(PostDetailsViewModel::class)
    abstract fun bindPostDetailsViewModel(viewModel: PostDetailsViewModel): ViewModel
}