package com.example.teqqed_test.ui.posts.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.teqqed_test.BR
import com.example.teqqed_test.databinding.PostDetailsFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class PostDetailsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<PostDetailsViewModel> { viewModelFactory }

    val args: PostDetailsFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = PostDetailsFragmentBinding.inflate(inflater, container, false).apply {
            setVariable(BR.model, viewModel.viewData)
            lifecycleOwner = viewLifecycleOwner
        }

        val adapter = CommentsAdapter(lifecycleOwner = viewLifecycleOwner)
        binding.rvComments.adapter = adapter
        binding.rvComments.addItemDecoration(
            DividerItemDecoration(
                binding.rvComments.context,
                DividerItemDecoration.VERTICAL
            )
        )
        subscribeUi(adapter)
        return binding.root
    }

    private fun subscribeUi(adapter: CommentsAdapter) {
        viewModel.viewData.commentsList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }
}