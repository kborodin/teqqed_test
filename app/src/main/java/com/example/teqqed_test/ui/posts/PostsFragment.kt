package com.example.teqqed_test.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.example.teqqed_test.databinding.PostsFragmentBinding
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class PostsFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<PostsViewModel> { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = PostsFragmentBinding.inflate(inflater, container, false)
            .apply { lifecycleOwner = this@PostsFragment }

        val adapter = PostsAdapter(lifecycleOwner = viewLifecycleOwner)
        binding.rvPosts.adapter = adapter
        binding.rvPosts.addItemDecoration(
            DividerItemDecoration(
                binding.rvPosts.context,
                DividerItemDecoration.VERTICAL
            )
        )
        subscribeUi(adapter)
        return binding.root
    }

    private fun subscribeUi(adapter: PostsAdapter) {
        viewModel.postsViewData.postsList.observe(viewLifecycleOwner, Observer {
            adapter.submitList(it)
        })
    }
}