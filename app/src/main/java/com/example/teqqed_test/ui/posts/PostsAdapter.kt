package com.example.teqqed_test.ui.posts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.teqqed_test.databinding.ItemPostBinding
import com.example.teqqed_test.model.domain.Post
import java.util.concurrent.Executors

class PostsAdapter(private val lifecycleOwner: LifecycleOwner) :
    ListAdapter<Post, PostsAdapter.PostsHolder>(asyncDifferConfig(DiffCallback)) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsHolder {
        return PostsHolder(
            ItemPostBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PostsHolder, position: Int) {
        val post = getItem(position)
        holder.apply {
            bind(createOnClickListener(post.id), getItem(position))
        }
    }

    private fun createOnClickListener(postId: Int): View.OnClickListener {
        return View.OnClickListener {
            val direction = PostsFragmentDirections.actionPostsFragmentToPostDetailsFragment(postId)
            it.findNavController().navigate(direction)
        }
    }

    inner class PostsHolder(private val binding: ItemPostBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(listener: View.OnClickListener, item: Post) {
            binding.apply {
                post = item
                clickListener = listener
                lifecycleOwner = this@PostsAdapter.lifecycleOwner
                executePendingBindings()
            }

        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Post>() {

        override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Post,
            newItem: Post
        ): Boolean =
            oldItem == newItem
    }
}

fun <T> asyncDifferConfig(diffCallback: DiffUtil.ItemCallback<T>) =
    AsyncDifferConfig.Builder(diffCallback)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()