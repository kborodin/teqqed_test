package com.example.teqqed_test.ui.posts

import androidx.lifecycle.MutableLiveData
import com.example.teqqed_test.model.domain.Post

data class PostsViewData(
    val postsList: MutableLiveData<List<Post>> = MutableLiveData()
)