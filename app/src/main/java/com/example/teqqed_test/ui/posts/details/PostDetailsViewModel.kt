package com.example.teqqed_test.ui.posts.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.teqqed_test.interactor.GetCommentsInteractor
import com.example.teqqed_test.interactor.GetPostInteractor
import com.example.teqqed_test.interactor.GetUserInteractor
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.launchIn
import timber.log.Timber
import javax.inject.Inject

class PostDetailsViewModel @Inject constructor(
    private val getUserInteractor: GetUserInteractor,
    private val getPostInteractor: GetPostInteractor,
    private val getCommentsInteractor: GetCommentsInteractor,
    private val postId: Int?
) : ViewModel() {

    val viewData by lazy { PostDetailsViewData() }

    init {
        getPost()
    }

    private fun getPost() {
        postId?.let {
            getPostInteractor.execute(it)
                .flatMapLatest { post ->
                    viewData.postTitle.postValue(post.title)
                    viewData.postBody.postValue(post.body)
                    combine(
                        getUser(post.userId),
                        getComments(post.id)
                    ) { user, comments ->
                        viewData.userName.postValue(user.name)
                        viewData.commentsList.postValue(comments)
                    }
                }
                .catch { throwable -> Timber.e(throwable, "Could not get required data") }
                .launchIn(viewModelScope)
        }
    }

    private fun getUser(userId: Int) = getUserInteractor.execute(userId)

    private fun getComments(postId: Int) = getCommentsInteractor.execute(postId)
}