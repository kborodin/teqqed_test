package com.example.teqqed_test.ui.posts.details

import androidx.lifecycle.MutableLiveData
import com.example.teqqed_test.model.domain.Comment

data class PostDetailsViewData(
    val postTitle: MutableLiveData<String> = MutableLiveData(),
    val postBody: MutableLiveData<String> = MutableLiveData(),
    val userName: MutableLiveData<String> = MutableLiveData(),
    val commentsList: MutableLiveData<List<Comment>> = MutableLiveData()
)