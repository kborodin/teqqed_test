package com.example.teqqed_test.ui.posts.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.teqqed_test.databinding.ItemCommentsBinding
import com.example.teqqed_test.model.domain.Comment
import java.util.concurrent.Executors

class CommentsAdapter(private val lifecycleOwner: LifecycleOwner) :
    ListAdapter<Comment, CommentsAdapter.CommentsHolder>(asyncDifferConfig(DiffCallback)) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsHolder {
        return CommentsHolder(
            ItemCommentsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CommentsHolder, position: Int) {
        holder.apply {
            bind(getItem(position))
        }
    }

    inner class CommentsHolder(private val binding: ItemCommentsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Comment) {
            binding.apply {
                comment = item
                lifecycleOwner = this@CommentsAdapter.lifecycleOwner
                executePendingBindings()
            }
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Comment>() {

        override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Comment,
            newItem: Comment
        ): Boolean =
            oldItem == newItem
    }
}

fun <T> asyncDifferConfig(diffCallback: DiffUtil.ItemCallback<T>) =
    AsyncDifferConfig.Builder(diffCallback)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()