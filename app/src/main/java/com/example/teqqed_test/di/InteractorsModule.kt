package com.example.teqqed_test.di

import com.example.teqqed_test.api.TeqqedApi
import com.example.teqqed_test.interactor.*
import com.example.teqqed_test.repository.comment.CommentsRepository
import com.example.teqqed_test.repository.post.PostsRepository
import com.example.teqqed_test.repository.user.UsersRepository
import dagger.Module
import dagger.Provides

@Module
object InteractorsModule {

    @Provides
    fun fetchDataInteractor(
        teqqedApi: TeqqedApi,
        postsRepository: PostsRepository,
        usersRepository: UsersRepository,
        commentsRepository: CommentsRepository
    ) =
        FetchDataInteractor(
            teqqedApi = teqqedApi,
            postsRepository = postsRepository,
            usersRepository = usersRepository,
            commentsRepository = commentsRepository
        )

    @Provides
    fun provideGetPostsInteractor(postsRepository: PostsRepository) =
        GetPostsInteractor(postsRepository)

    @Provides
    fun provideGetUserInteractor(usersRepository: UsersRepository) =
        GetUserInteractor(usersRepository)

    @Provides
    fun provideGetPostInteractor(postsRepository: PostsRepository) =
        GetPostInteractor(postsRepository)

    @Provides
    fun provideGetCommentsInteractor(commentsRepository: CommentsRepository) =
        GetCommentsInteractor(commentsRepository)
}