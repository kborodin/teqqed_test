package com.example.teqqed_test.di

import com.example.teqqed_test.ui.posts.PostsFragment
import com.example.teqqed_test.ui.posts.details.PostDetailsFragment
import com.example.teqqed_test.ui.posts.details.PostDetailsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Suppress("unused")
@Module
abstract class FragmentBindingsModule {
    @ContributesAndroidInjector
    abstract fun contributePostsFragment(): PostsFragment

    @ContributesAndroidInjector(modules = [PostDetailsModule::class])
    abstract fun contributePostDetailsFragment(): PostDetailsFragment
}