package com.example.teqqed_test.di

import com.example.teqqed_test.AppDispatchers
import com.example.teqqed_test.api.TeqqedApi
import com.example.teqqed_test.api.TeqqedApiImpl
import com.example.teqqed_test.service.TeqqedService
import dagger.Module
import dagger.Provides

@Module
object ApiModule {

    @Provides
    fun provideTeqqedApi(service: TeqqedService, appDispatchers: AppDispatchers): TeqqedApi =
        TeqqedApiImpl(service, appDispatchers)
}