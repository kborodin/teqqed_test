package com.example.teqqed_test.di

import android.app.Application
import androidx.room.Room
import com.example.teqqed_test.db.AppDatabase
import dagger.Module
import dagger.Provides

@Module
object DatabaseModule {

    @Provides
    fun provideDb(app: Application) =
        Room.databaseBuilder(app, AppDatabase::class.java, "teqqed-db").build()

    @Provides
    fun providePostsDao(appDatabase: AppDatabase) = appDatabase.postsDao()

    @Provides
    fun provideUsersDao(appDatabase: AppDatabase) = appDatabase.usersDao()

    @Provides
    fun provideCommentsDao(appDatabase: AppDatabase) = appDatabase.commentsDao()
}