package com.example.teqqed_test.di

import com.example.teqqed_test.db.dao.CommentsDao
import com.example.teqqed_test.db.dao.PostsDao
import com.example.teqqed_test.db.dao.UsersDao
import com.example.teqqed_test.repository.comment.CommentsRepository
import com.example.teqqed_test.repository.comment.CommentsRepositoryImpl
import com.example.teqqed_test.repository.post.PostsRepository
import com.example.teqqed_test.repository.post.PostsRepositoryImpl
import com.example.teqqed_test.repository.user.UsersRepository
import com.example.teqqed_test.repository.user.UsersRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
object RepositoryModule {

    @Provides
    fun providePostsRepository(postsDao: PostsDao): PostsRepository =
        PostsRepositoryImpl(postsDao)

    @Provides
    fun provideUsersRepository(usersDao: UsersDao): UsersRepository =
        UsersRepositoryImpl(usersDao)

    @Provides
    fun provideCommentsRepository(commentsDao: CommentsDao): CommentsRepository =
        CommentsRepositoryImpl(commentsDao)
}