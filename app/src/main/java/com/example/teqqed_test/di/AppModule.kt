package com.example.teqqed_test.di

import com.example.teqqed_test.AppDispatchers
import com.example.teqqed_test.DefaultAppDispatchers
import com.example.teqqed_test.service.TeqqedService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class, CoreDataModule::class, ApiModule::class, DatabaseModule::class, InteractorsModule::class, RepositoryModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideTeqqedService(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ) =
        provideService(okHttpClient, converterFactory, TeqqedService::class.java)

    @Provides
    @Singleton
    fun appDispatchers(): AppDispatchers = DefaultAppDispatchers()

    @Provides
    fun provideOkHttpClient(interceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()

    private fun <T> provideService(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory, clazz: Class<T>
    ): T {
        return createRetrofit(okHttpClient, converterFactory).create(clazz)
    }

    private fun createRetrofit(
        okHttpClient: OkHttpClient,
        converterFactory: GsonConverterFactory
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(TeqqedService.ENDPOINT)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }
}