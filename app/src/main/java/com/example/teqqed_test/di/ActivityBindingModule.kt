package com.example.teqqed_test.di

import com.example.teqqed_test.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class ActivityBindingModule {
    @ContributesAndroidInjector(modules = [FragmentBindingsModule::class])
    abstract fun contributeMainActivity(): MainActivity
}