package com.example.teqqed_test.model.domain

data class Comment(
    val id: Int,
    val postId: Int,
    val name: String,
    val body: String,
    val email: String
)