package com.example.teqqed_test.model.api

import com.google.gson.annotations.SerializedName

data class UserModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("name")
    val name: String
)