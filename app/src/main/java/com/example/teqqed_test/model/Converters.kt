package com.example.teqqed_test.model

import com.example.teqqed_test.model.api.CommentModel
import com.example.teqqed_test.model.api.PostModel
import com.example.teqqed_test.model.api.UserModel
import com.example.teqqed_test.model.datastorage.CommentData
import com.example.teqqed_test.model.datastorage.PostData
import com.example.teqqed_test.model.datastorage.UserData
import com.example.teqqed_test.model.domain.Comment
import com.example.teqqed_test.model.domain.Post
import com.example.teqqed_test.model.domain.User

fun PostModel.toDomain() =
    Post(
        id = id,
        userId = userId,
        title = title,
        body = body
    )

fun PostData.toDomain() =
    Post(
        id = id,
        userId = userId,
        title = title,
        body = body
    )

fun Post.toDataStorage() =
    PostData(
        id = id,
        userId = userId,
        title = title,
        body = body
    )

fun UserModel.toDomain() =
    User(
        id = id,
        name = name
    )

fun UserData.toDomain() =
    User(
        id = id,
        name = name
    )

fun User.toDataStorage() =
    UserData(
        id = id,
        name = name
    )


fun CommentModel.toDomain() =
    Comment(
        id = id,
        postId = postId,
        name = name,
        body = body,
        email = email
    )

fun CommentData.toDomain() =
    Comment(
        id = id,
        postId = postId,
        name = name,
        body = body,
        email = email
    )

fun Comment.toDataStorage() =
    CommentData(
        id = id,
        postId = postId,
        name = name,
        body = body,
        email = email
    )
