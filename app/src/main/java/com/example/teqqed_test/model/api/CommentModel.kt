package com.example.teqqed_test.model.api

import com.google.gson.annotations.SerializedName

data class CommentModel(
    @SerializedName("id")
    val id: Int,
    @SerializedName("postId")
    val postId: Int,
    @SerializedName("name")
    val name: String,
    @SerializedName("body")
    val body: String,
    @SerializedName("email")
    val email: String
)