package com.example.teqqed_test.model.domain

data class User(
    val id: Int,
    val name: String
)