package com.example.teqqed_test.repository.comment

import com.example.teqqed_test.db.dao.CommentsDao
import com.example.teqqed_test.model.domain.Comment
import com.example.teqqed_test.model.toDataStorage
import com.example.teqqed_test.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CommentsRepositoryImpl @Inject constructor(
    private val commentsDao: CommentsDao
) : CommentsRepository {

    override suspend fun save(commentsList: List<Comment>) =
        commentsDao.insert(commentsList.map { it.toDataStorage() })

    override fun getCommentsByPostId(postId: Int): Flow<List<Comment>> =
        commentsDao.getCommentsByPostId(postId).filterNotNull()
            .map { list -> list.map { it.toDomain() } }
}