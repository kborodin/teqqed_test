package com.example.teqqed_test.repository.post

import com.example.teqqed_test.model.domain.Post
import kotlinx.coroutines.flow.Flow

interface PostsRepository {

    suspend fun save(postsList: List<Post>)

    fun getPosts(): Flow<List<Post>>

    fun getPostById(postId: Int): Flow<Post>
}