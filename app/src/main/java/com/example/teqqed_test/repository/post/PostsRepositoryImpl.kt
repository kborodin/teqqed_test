package com.example.teqqed_test.repository.post

import com.example.teqqed_test.db.dao.PostsDao
import com.example.teqqed_test.model.domain.Post
import com.example.teqqed_test.model.toDataStorage
import com.example.teqqed_test.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class PostsRepositoryImpl @Inject constructor(
    private val postsDao: PostsDao
) : PostsRepository {

    override suspend fun save(postsList: List<Post>) =
        postsDao.insert(postsList.map { it.toDataStorage() })

    override fun getPosts(): Flow<List<Post>> =
        postsDao.getPosts().filterNotNull().map { list -> list.map { it.toDomain() } }

    override fun getPostById(postId: Int): Flow<Post> =
        postsDao.getPostById(postId).map { it.toDomain() }
}