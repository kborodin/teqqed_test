package com.example.teqqed_test.repository.user

import com.example.teqqed_test.db.dao.UsersDao
import com.example.teqqed_test.model.domain.User
import com.example.teqqed_test.model.toDataStorage
import com.example.teqqed_test.model.toDomain
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class UsersRepositoryImpl @Inject constructor(
    private val usersDao: UsersDao
) : UsersRepository {

    override suspend fun save(usersList: List<User>) =
        usersDao.insert(usersList.map { it.toDataStorage() })

    override fun getUserById(userId: Int): Flow<User> =
        usersDao.getUserById(userId).map { it.toDomain() }
}