package com.example.teqqed_test.repository.user

import com.example.teqqed_test.model.domain.User
import kotlinx.coroutines.flow.Flow

interface UsersRepository {

    suspend fun save(usersList: List<User>)

    fun getUserById(userId: Int): Flow<User>
}