package com.example.teqqed_test.repository.comment

import com.example.teqqed_test.model.domain.Comment
import kotlinx.coroutines.flow.Flow

interface CommentsRepository {

    suspend fun save(commentsList: List<Comment>)

    fun getCommentsByPostId(postId: Int): Flow<List<Comment>>
}